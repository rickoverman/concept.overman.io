<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Twitter Boostrap for Demo -->
        <link rel="stylesheet" href="http:////maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <!-- styles for video player -->
        <link rel="stylesheet" href="src/style.css">
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>


        <div id="background-video" class="background-video">
          <img src="images/placeholder.jpg" alt="" class="placeholder-image">
        </div>



          <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
          <!-- ADD Jquery Video Background -->
          <script src="src/jquery.youtubebackground.js"></script>
          <script>
            jQuery(function($) {


              $('#background-video').YTPlayer({
                fitToBackground: true,
                videoId: 'jQRI3b2SX8c',
                pauseOnScroll: true,
                mute:false,
                callback: function() {
                  videoCallbackEvents();



                }
              });

              var videoCallbackEvents = function() {
                var player = $('#background-video').data('ytPlayer').player;

                player.addEventListener('onStateChange', function(event){
                    console.log("Player State Change", event);

                    // OnStateChange Data
                    if (event.data === 0) {
                        console.log('video ended');
                    }
                    else if (event.data === 2) {
                      console.log('paused');
                    }
                });
              }
            });
          </script>
    </body>
</html>
