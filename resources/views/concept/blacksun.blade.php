<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>overman.io | Rick Overman, at your service!</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/prettyPhoto.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="src/style.css">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" type="image/png" href="/images/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body data-spy="scroll" data-target="#navbar" data-offset="0">
    <header id="header" role="banner">
        <div class="container">
            <div id="navbar" class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="text-big" href="index.html"><img src="/images/logo-dgtl.png"></div>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#main-slider"><i class="icon-home"></i></a></li>
                        <li><a href="#services">Services</a></li>
                        <li><a href="#portfolio">Portfolio</a></li>
                        <li><a href="#pricing">Pricing</a></li>
                        <li><a href="#about-us">About Me</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header><!--/#header-->

    <div id="background-video" class="background-video">
      <img src="images/placeholder.jpg" alt="" class="placeholder-image">
    </div>

    <section id="main-slider" class="carousel">
        <div class="carousel-inner">
            <div class="item ">
                <div class="container">
                    <div class="carousel-content">

                            <a href="/" class=""><img width="240" src="/images/boiler-rum.png"></a>

                            </div>
                </div>
            </div><!--/.item-->
            <div class="item ">
                <div class="container">
                    <div class="carousel-content">

                    </div>
                </div>
            </div><!--/.item-->
            <div class="item active">
                <div class="container">
                    <div class="carousel-content">
                        <a href="/blacksun" class=""><img width="350" src="/images/maxresdefault.png"></a>

                    </div>
                </div>
            </div><!--/.item-->
            <div class="item ">
                <div class="container">
                    <div class="carousel-content">
                        <a href="/timewarp" class=""><img style="margin-top: 101px;" width="350" src="/images/timewarp.png"></a>

                    </div>
                </div>
            </div><!--/.item-->
        </div><!--/.carousel-inner-->
        <a class="prev" href="#main-slider" data-slide="prev"><i class="icon-angle-left"></i></a>
        <a class="next" href="#main-slider" data-slide="next"><i class="icon-angle-right"></i></a>
    </section><!--/#main-slider-->

    <section id="services">
        <div class="container">
            <div class="box first">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="center">
                            <i class="icon-apple icon-md icon-color1"></i>
                            <h4>WordPress Theme development</h4>
                            <p>I offer premade and custom wordpress templates tailored for your company.</p>
                        </div>
                    </div><!--/.col-md-4-->
                    <div class="col-md-4 col-sm-6">
                        <div class="center">
                            <i class="icon-android icon-md icon-color2"></i>
                            <h4>Mobile App development</h4>
                            <p>I offer mobile development services for Android and iOS that make use of the latest frameworks and technology standards.</p>
                        </div>
                    </div><!--/.col-md-4-->
                    <div class="col-md-4 col-sm-6">
                        <div class="center">
                            <i class="icon-windows icon-md icon-color3"></i>
                            <h4>Web development</h4>
                            <p>I offer web development services that make use of the latest frameworks and technology standards.<br><br></p>
                        </div>
                    </div><!--/.col-md-4-->
                    <div class="col-md-4 col-sm-6">
                        <div class="center">
                            <i class="icon-html5 icon-md icon-color4"></i>
                            <h4>Open Source projects</h4>
                            <p>My open source projects for usefull examples and to show my coding style in diffrent languages.</p>
                        </div>
                    </div><!--/.col-md-4-->
                    <div class="col-md-4 col-sm-6">
                        <div class="center">
                            <i class="icon-css3 icon-md icon-color5"></i>
                            <h4>Graphic & UX design</h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                        </div>
                    </div><!--/.col-md-4-->
                    <div class="col-md-4 col-sm-6">
                        <div class="center">
                            <i class="icon-thumbs-up icon-md icon-color6"></i>
                            <h4>VR & Game development</h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                        </div>
                    </div><!--/.col-md-4-->
                </div><!--/.row-->
            </div><!--/.box-->
        </div><!--/.container-->
    </section><!--/#services-->

    <section id="portfolio">
        <div class="container">
            <div class="box">
                <div class="center gap">
                    <h2>Portfolio</h2>
                    <p class="lead">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac<br>turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                </div><!--/.center-->
                <ul class="portfolio-filter">
                    <li><a class="btn btn-primary active" href="#" data-filter="*">All</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".bootstrap">Bootstrap</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".bootstrap">Foundation</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".html">HTML Templates</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".wordpress">Wordpress</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".wordpress">NodeJS</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".wordpress">Laravel</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".wordpress">UX Designs</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".wordpress">Linux</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".wordpress">Mobile Apps</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".wordpress">PlayCanvas</a></li>
                </ul><!--/#portfolio-filter-->
                <ul class="portfolio-items col-4">
                    <li class="portfolio-item apps">
                        <div class="item-inner">
                            <div class="portfolio-image">
                                <img src="/images/portfolio/thumb/item1.jpg" alt="">
                                <div class="overlay">
                                    <a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="images/portfolio/full/item1.jpg"><i class="icon-eye-open"></i></a>
                                </div>
                            </div>
                            <h5>Lorem ipsum dolor sit amet</h5>
                        </div>
                    </li><!--/.portfolio-item-->
                    <li class="portfolio-item joomla bootstrap">
                        <div class="item-inner">
                            <div class="portfolio-image">
                                <img src="/images/portfolio/thumb/item2.jpg" alt="">
                                <div class="overlay">
                                    <a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="images/portfolio/full/item2.jpg"><i class="icon-eye-open"></i></a>
                                </div>
                            </div>
                            <h5>Lorem ipsum dolor sit amet</h5>
                        </div>
                    </li><!--/.portfolio-item-->
                    <li class="portfolio-item bootstrap wordpress">
                        <div class="item-inner">
                            <div class="portfolio-image">
                                <img src="/images/portfolio/thumb/item3.jpg" alt="">
                                <div class="overlay">
                                    <a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="images/portfolio/full/item3.jpg"><i class="icon-eye-open"></i></a>
                                </div>
                            </div>
                            <h5>Lorem ipsum dolor sit amet</h5>
                        </div>
                    </li><!--/.portfolio-item-->
                    <li class="portfolio-item joomla wordpress apps">
                        <div class="item-inner">
                            <div class="portfolio-image">
                                <img src="/images/portfolio/thumb/item4.jpg" alt="">
                                <div class="overlay">
                                    <a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="images/portfolio/full/item4.jpg"><i class="icon-eye-open"></i></a>
                                </div>
                            </div>
                            <h5>Lorem ipsum dolor sit amet</h5>
                        </div>
                    </li><!--/.portfolio-item-->
                    <li class="portfolio-item joomla html">
                        <div class="item-inner">
                            <div class="portfolio-image">
                                <img src="/images/portfolio/thumb/item5.jpg" alt="">
                                <div class="overlay">
                                    <a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="images/portfolio/full/item5.jpg"><i class="icon-eye-open"></i></a>
                                </div>
                            </div>
                            <h5>Lorem ipsum dolor sit amet</h5>
                        </div>
                    </li><!--/.portfolio-item-->
                    <li class="portfolio-item wordpress html">
                        <div class="item-inner">
                            <div class="portfolio-image">
                                <img src="/images/portfolio/thumb/item6.jpg" alt="">
                                <div class="overlay">
                                    <a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="images/portfolio/full/item6.jpg"><i class="icon-eye-open"></i></a>
                                </div>
                            </div>
                            <h5>Lorem ipsum dolor sit amet</h5>
                        </div>
                    </li><!--/.portfolio-item-->
                    <li class="portfolio-item joomla html">
                        <div class="item-inner">
                            <div class="portfolio-image">
                                <img src="/images/portfolio/thumb/item5.jpg" alt="">
                                <div class="overlay">
                                    <a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="images/portfolio/full/item5.jpg"><i class="icon-eye-open"></i></a>
                                </div>
                            </div>
                            <h5>Lorem ipsum dolor sit amet</h5>
                        </div>
                    </li><!--/.portfolio-item-->
                    <li class="portfolio-item wordpress html">
                        <div class="item-inner">
                            <div class="portfolio-image">
                                <img src="/images/portfolio/thumb/item6.jpg" alt="">
                                <div class="overlay">
                                    <a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="images/portfolio/full/item6.jpg"><i class="icon-eye-open"></i></a>
                                </div>
                            </div>
                            <h5>Lorem ipsum dolor sit amet</h5>
                        </div>
                    </li><!--/.portfolio-item-->
                </ul>
            </div><!--/.box-->
        </div><!--/.container-->
    </section><!--/#portfolio-->

    <section id="pricing">
        <div class="container">
            <div class="box">
                <div class="center">
                    <h2>See my pricings</h2>
                    <p class="lead">I always try to offer you fair and competitive prices against the current market.</p>
                    <p class=""><i>All prices are displayed before tax.</i></p>
                </div><!--/.center-->
                <div class="big-gap"></div>
                <div id="pricing-table" class="row">
                    <div class="col-sm-3">
                        <ul class="plan">
                            <li class="plan-name">Hourly Rate</li>
                            <li class="plan-price">&euro;35</li>
                            <li>Premium Support</li>
                            <li>High Quality Code </li>
                            <li>Responsive Website development</li>
                            <li>Mobile App development</li>
                            <li>UX/Template design</li>
                            <li>Game development</li>
                            <li>Project Documentation</li>
                            <li>Latest technology</li>
                            <li>Kennismaking op locatie</li>
                            <li>Fast Delivery</li>
                            <li class="plan-action"><a href="#" class="btn btn-primary btn-lg">Signup</a></li>
                        </ul>
                    </div><!--/.col-sm-4-->
                    <div class="col-sm-3">
                        <ul class="plan featured">
                            <li class="plan-name">Website or App with CMS</li>
                            <li class="plan-price">&euro;840</li>
                            <li><b>&euro;20 voucher</b> for responsive<br>front-end templates</li>
                            <li><b>&euro;40 voucher</b> for Premium Support</li>
                            <li>High Quality Code </li>

                            <li>WordPress (optional)</li>
                            <li>Contact Form</li>
                            <li>Search</li>
                            <li>Social media sharing</li>
                            <li>Google Analytics</li>
                            <li>SEO ready</li>
                            <li>About 5 pages</li>
                            <li>Extra data object (+ &euro;100)</li>
                            <li>Kennismaking op locatie</li>
                            <li>Fast Delivery (whithin a week)</li>
                            <li class="plan-action"><a href="#" class="btn btn-primary btn-lg">Signup</a></li>
                        </ul>
                    </div><!--/.col-sm-4-->
                    <div class="col-sm-3">
                        <ul class="plan">
                            <li class="plan-name">Responsive Template</li>
                            <li class="plan-price">&euro;70</li>
                            <li>HTML5 / CSS3</li>
                            <li>W3C validated</li>
                            <li>High Quality Code </li>

                            <li>Secure compressed Javascript</li>
                            <li>Nodejs & Browserify</li>
                            <li>Bootstrap / Foundation</li>
                            <li>Sass / Compas</li>
                            <li>SEO ready</li>
                            <li>Project Documentation</li>
                            <li>Photoshop Design Files</li>
                            <li>Fast Direct Download</li>
                            <li class="plan-action"><a href="#" class="btn btn-primary btn-lg">Signup</a></li>
                        </ul>
                    </div><!--/.col-sm-4-->
                    <div class="col-sm-3">
                        <ul class="plan">
                            <li class="plan-name">Premium Support</li>
                            <li class="plan-price">&euro;40</li>
                            <li>Email & Hosting Services</li>
                            <li>4 hour Premium Support</li>
                            <li><b>&euro;10 discount</b> on hourly rates</li>
                            <li>Gratis software-installatie voor: <i>WordPress, Joomla!, MijnWebsite, OpenCart, phpBB, PrestaShop, Coppermine, MediaWiki, Drupal, phpMyFAQ, phpList, Gbook, Lazarus</i></li>
                            <li>Fast response times</li>
                            <li>High Priority</li>
                            <li>Berijkbaar in de weekend en avonden</li>
                            <li>SEO maintanence</li>
                            <li>Software Updates</li>
                            <li class="plan-action"><a href="#" class="btn btn-primary btn-lg">Signup</a></li>
                        </ul>
                    </div><!--/.col-sm-4-->
                </div>
            </div>
        </div>
    </section><!--/#pricing-->

    <section id="about-us">
        <div class="container">
            <div class="box">
                <div class="center">
                    <h2>Meet me</h2>
                    <p class="lead">Please contact me for any questions or tailored offers.</p>
                </div>
                <div class="gap"></div>
                <div id="team-scroller" class="carousel scale">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="member">
                                        <p><img class="img-responsive img-thumbnail img-circle" src="/images/team1.jpg" alt="" ></p>
                                        <h3>Agnes Smith<small class="designation">CEO &amp; Founder</small></h3>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="member">
                                        <p><img class="img-responsive img-thumbnail img-circle" src="/images/rickyrickme.jpg" alt="" ></p>
                                        <h3>Rick Overman<small class="designation">Designer & Developer at your service!</small></h3>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="member">
                                        <p><img class="img-responsive img-thumbnail img-circle" src="/images/team3.jpg" alt="" ></p>
                                        <h3>David Robbins<small class="designation">Co-Founder</small></h3>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="member">
                                        <p><img class="img-responsive img-thumbnail img-circle" src="/images/team1.jpg" alt="" ></p>
                                        <h3>Philip Mejia<small class="designation">Marketing Manager</small></h3>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="member">
                                        <p><img class="img-responsive img-thumbnail img-circle" src="/images/team2.jpg" alt="" ></p>
                                        <h3>Charles Erickson<small class="designation">Support Manager</small></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left-arrow" href="#team-scroller" data-slide="prev">
                        <i class="icon-angle-left icon-4x"></i>
                    </a>
                    <a class="right-arrow" href="#team-scroller" data-slide="next">
                        <i class="icon-angle-right icon-4x"></i>
                    </a>
                </div><!--/.carousel-->
            </div><!--/.box-->
        </div><!--/.container-->
    </section><!--/#about-us-->

    <section id="contact">
        <div class="container">
            <div class="box last">
                <div class="row">
                    <div class="col-sm-6">
                        <h1>Contact Form</h1>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                        <div class="status alert alert-success" style="display: none"></div>
                        <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="sendemail.php" role="form">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" required="required" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" required="required" placeholder="Email address">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Message"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-danger btn-lg">Send Message</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!--/.col-sm-6-->
                    <div class="col-sm-6">
                        <h1>My Details</h1>
                        <div class="row">
                            <div class="col-md-6">
                                <address>
                                    <strong>Rick Overman</strong><br>
                                    Liornestraat 73<br>
                                    1624HJ Hoorn<br>
                                </address>
                            </div>
                            <div class="col-md-6">
                                <address>
                                    <abbr title="Phone">P:</abbr> (+31) 6 117 506 85 <br>
                                    <abbr title="Phone">M:</abbr> ricky.overman@gmail.com

                                </address>
                            </div>
                        </div>
                        <h1>My Resources</h1>
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="social">
                                    <li><a href="#"><i class="icon-facebook icon-social"></i> Share on Facebook</a></li>
                                    <li><a href="#"><i class="icon-twitter icon-social"></i> Share on Twitter</a></li>
                                    <li><a target="_blank" href="http://www.stumbleupon.com/stumbler/rickyoverman/lists"><i class="icon-youtube icon-social"></i> Stumbleupon</a></li>
                                    <li><a href="#"><i class="icon-pinterest icon-social"></i> Github</a></li>
                                    <li><a href="#"><i class="icon-youtube icon-social"></i> Youtube</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="social">
                                    <li><a href="#"><i class="icon-linkedin icon-social"></i> Linkedin</a></li>
                                    <li><a href="#"><i class="icon-twitter icon-social"></i> Twitter</a></li>

                                </ul>
                            </div>
                        </div>
                    </div><!--/.col-sm-6-->
                </div><!--/.row-->
            </div><!--/.box-->
        </div><!--/.container-->
    </section><!--/#contact-->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2013 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <img class="pull-right" src="/images/shapebootstrap.png" alt="ShapeBootstrap" title="ShapeBootstrap">
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.isotope.min.js"></script>
    <script src="/js/jquery.prettyPhoto.js"></script>
    <script src="/js/main.js"></script>
    <!-- ADD Jquery Video Background -->
    <script src="src/jquery.youtubebackground.js"></script>
    <script>
      jQuery(function($) {


        $('#background-video').YTPlayer({
          fitToBackground: true,
          videoId: 'hgOzpEO47ZI',
          pauseOnScroll: false,
          mute:false,
          callback: function() {
            videoCallbackEvents();



          }
        });

        var videoCallbackEvents = function() {
          var player = $('#background-video').data('ytPlayer').player;

          player.addEventListener('onStateChange', function(event){
              console.log("Player State Change", event);

              // OnStateChange Data
              if (event.data === 0) {
                  console.log('video ended');
              }
              else if (event.data === 2) {
                console.log('paused');
              }
          });
        }
      });
    </script>
</body>
</html>
